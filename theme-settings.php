<?php

/**
 * @file
 * Handles the custom theme settings
 */

/**
 * Return the theme settings' default values from the .info and save them into the database.
 * Credit: Zen http://drupal.org/project/zen
 *
 * @param $theme
 *   The name of theme.
 */
function ladybug_theme_get_default_settings($theme) {
 $themes = list_themes();

 // Get the default values from the .info file.
 $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();

 if (!empty($defaults)) {
   // Merge the defaults with the theme settings saved in the database.
   $settings = array_merge($defaults, variable_get('theme_'. $theme .'_settings', array()));
   // Save the settings back to the database.
   variable_set('theme_'. $theme .'_settings', $settings);
   // If the active theme has been loaded, force refresh of Drupal internals.
   if (!empty($GLOBALS['theme_key'])) {
     theme_get_setting('', TRUE);
   }
 }

 // Return the default settings.
 return $defaults;
}

/**
 * Implementation of _settings() theme function.
 *
 * @return array
 */
function ladybug_settings($saved_settings) {

  // Get the default settings.
  $defaults = ladybug_theme_get_default_settings('ladybug');
  // Merge the variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Breadcrumb Settings
  $form['ladybug_breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Breadcrumbs'),
    '#default_value' =>  $settings['ladybug_breadcrumbs'],
  );

  // Breadcrumb Separator
  $breadcrumbs_status = $settings['ladybug_breadcrumbs'] ? TRUE : FALSE;
  $breadcrumbs_desc = $breadcrumbs_status ? t('Select a breadcrumb separator.') : t('Breadcrumbs must be enabled to use this feature.');

  $form['ladybug_breadcrumbs_sep'] = array(
    '#type' => 'select',
    '#title' => t('Breadcrumb Separator'),
    '#default_value' =>  $settings['ladybug_breadcrumbs_sep'],
    '#options' => array(
      '&raquo;' => '»',
      '&rsaquo;' => '›',
      '&rarr;' => '→',
      '/' => t('/'),
    ),
    '#description' => $breadcrumbs_desc,
    '#disabled' => $breadcrumbs_status,
  );

  // Layout Options
  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => 'Layout Options',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Width of Dropdown menus
  $form['layout']['ladybug_sub_navigation_width'] = array(
    '#type' => 'select',
    '#title' => t('Dropdown Menus Second Level Menu Width'),
    '#default_value' => $settings['ladybug_sub_navigation_width'],
    '#options' => ladybug_size_range(10, 30, 'em', 15),
    '#description' => t('The drop-down menus need a width. IF you find your menu items need to be adjusted smaller or larger, you can tweak the settings here.'),
  );

  // Colors
  $form['colors'] = array(
    '#type' => 'fieldset',
    '#title' => 'Color Options',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Fonts
  $form['fonts'] = array(
    '#type' => 'fieldset',
    '#title' => 'Font Options',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Base Font Size
  $form['fonts']['ladybug_font_size'] = array(
    '#type' => 'select',
    '#title' => t('Base Font Size'),
    '#default_value' => $settings['ladybug_font_size'],
    '#options' => ladybug_size_range(11, 16, 'px', 12),
    '#description' => t('Select the base font size for the theme.'),
  );

  // Base Font
  $form['fonts']['ladybug_font'] = array(
    '#type' => 'select',
    '#title' => t('Base Font'),
    '#default_value' =>  $settings['ladybug_font'],
    '#options' => ladybug_font_list(),
    '#description' => t('Select the base font for the theme.'),
  );

  // Headings Font
  $form['fonts']['ladybug_font_headings'] = array(
    '#type' => 'select',
    '#title' => t('Headings Font'),
    '#default_value' =>  $settings['ladybug_font_headings'],
    '#options' => ladybug_font_list(),
    '#description' => t('Select the base font for the heading (block, page titles and heading tags).'),
  );

  // Links
  $form['colors']['ladybug_links'] = array(
    '#type' => 'textfield',
    '#title' => t('Links: Normal'),
    '#default_value' => $settings['ladybug_links'],
    '#description' => t('Example: <code>#314C74</code> or <code>blue</code> NOTE: This will only change the links that are blue by default.'),
  );

  // Links: Active
  $form['colors']['ladybug_links_active'] = array(
    '#type' => 'textfield',
    '#title' => t('Links: Active'),
    '#default_value' => $settings['ladybug_links_active'],
    '#description' => t('Example: <code>#314C74</code> or <code>blue</code> NOTE: This will only change the links that are blue by default'),
  );

  // Links: Hover
  $form['colors']['ladybug_links_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('Links: Hover'),
    '#default_value' => $settings['ladybug_links_hover'],
    '#description' => t('Example: <code>#314C74</code> or <code>blue</code> NOTE: This will only change the links that are blue by default'),
  );

  // Links: Visited
  $form['colors']['ladybug_links_visited'] = array(
    '#type' => 'textfield',
    '#title' => t('Links: Visited'),
    '#default_value' => $settings['ladybug_links_visited'],
    '#description' => t('Example: <code>#314C74</code> or <code>blue</code> NOTE: This will only change the links that are blue by default'),
  );

  // Generate custom.css and display a link to the file
  $form['ladybug_css'] = array(
    '#type' => 'fieldset',
    '#title' => 'Custom CSS Generation',
    '#description' =>  ladybug_write_css(), // This is the function that creates the custom.css file is created... Do not remove.
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  return $form;
}

function ladybug_build_css() {
  // Grab the current theme settings
  $theme_settings = variable_get('theme_ladybug_settings', '');
  if (!empty($theme_settings)) {
    // Build an array of only the theme related settings
    $setting = array();
    foreach ($theme_settings as $key => $value) {
      if (strpos($key, 'ladybug_') !== FALSE) {
        $setting[$key] = $value;
      }
    }
    // Handle custom settings for each case
    $custom_css = array();
    foreach ($setting as $key => $value) {
      switch ($key) {
        // Layout
        case 'ladybug_sub_navigation_size':
          $custom_css[] = '#navigation ul ul, #navigation ul ul li  { width: '. $value .'; }';
          $custom_css[] = '#navigation li .expanded ul { margin: -2.65em 0 0 '. $value .'!important; }';
          break;

        // Colors
        case 'ladybug_links':
          if (!empty($value)) {
            $custom_css[] = 'a { color: '. $value .'; }';
	  }
          break;
        case 'ladybug_links_hover':
          if (!empty($value)) {
            $custom_css[] = 'a:hover, a:visited:hover { color: '. $value .'; }';
	  }
          break;
        case 'ladybug_links_active':
          if (!empty($value)) {
            $custom_css[] = 'a.active, li a.active { color: '. $value .'; }';
	  }
          break;
        case 'ladybug_links_visited':
          if (!empty($value)) {
            $custom_css[] = 'a:visited { color: '. $value .'; }';
	  }
          break;

        // Fonts
        case 'ladybug_font_size':
          if (!empty($value)) {
            $custom_css[] = '#wrapper { font-size: '. $value .'; }';
	  }
          break;
        case 'ladybug_font':
          $custom_css[] = 'html, body, .form-radio, .form-checkbox, .form-file, .form-select, select, .form-text, input, .form-textarea, textarea  { font-family: '. ladybug_font_stack($value) .'; }';
          break;
        case 'ladybug_font_headings':
          $custom_css[] = 'h1, h2, h3, h4, h5, h6  { font-family: '. ladybug_font_stack($value) .'; }';
          break;
      }
    }
    return implode("\r\n", $custom_css);
  }
}

function ladybug_write_css() {
  // Set the location of the custom CSS file
  $file_path = file_directory_path() .'/css/ladybug.css';

  // If the directory doesn't exist, create it
  file_check_directory(dirname($file_path), FILE_CREATE_DIRECTORY);

  // Generate the CSS
  $file_contents = ladybug_build_css();
  $output = '<div class="description">'. t('This CSS is generated by the settings chosen above and placed in the files directory: '. l($file_path, $file_path) .'. The file is generated each time this page (and only this page) is loaded. <strong class="marker">Make sure to refresh your page to see the changes</strong>') .'</div>';

  file_save_data($file_contents, $file_path, FILE_EXISTS_REPLACE);
  drupal_flush_all_caches();

  return $output;

}

/**
 * Helper function to provide a list of fonts for select list in theme settings.
 */
function ladybug_font_list() {
  $fonts = array(
    'Sans-serif' => array(
      'verdana' => t('Verdana'),
      'helvetica' => t('Helvetica, Arial'),
      'lucida' => t('Lucida Grande, Lucida Sans Unicode'),
      'geneva' => t('Geneva'),
      'tahoma' => t('Tahoma'),
      'century' => t('Century Gothic'),
    ),
    'Serif' => array(
      'georgia' => t('Georgia'),
      'palatino' => t('Palatino Linotype, Book Antiqua'),
      'times' => t('Times New Roman'),
    ),
  );
  return $fonts;
}

/**
 * Provides Font Stack values for theme settings which are written to custom.css
 * @see ladybug_font_list()
 * @param $attributes
 * @return string
 */
function ladybug_font_stack($font) {
  if ($font) {
    $fonts = array(
      'verdana' => '"Bitstream Vera Sans", Verdana, Arial, sans-serif',
      'helvetica' => 'Helvetica, Arial, "Nimbus Sans L", "Liberation Sans", "FreeSans", sans-serif',
      'lucida' => '"Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", "DejaVu Sans", Arial, sans-serif',
      'geneva' => '"Geneva", "Bitstream Vera Serif", "Tahoma", sans-serif',
      'tahoma' => 'Tahoma, Geneva, "DejaVu Sans Condensed", sans-serif',
      'century' => '"Century Gothic", "URW Gothic L", Helvetica, Arial, sans-serif',
      'georgia' => 'Georgia, "Bitstream Vera Serif", serif',
      'palatino' => '"Palatino Linotype", "URW Palladio L", "Book Antiqua", "Palatino", serif',
      'times' => '"Free Serif", "Times New Roman", Times, serif',
    );

    foreach ($fonts as $key => $value) {
      if ($font == $key) {
        $output = $value;
      }
    }
  }
  return $output;
}

/**
 * Helper function to provide a list of sizes for use in theme settings.
 */
function ladybug_size_range($start = 11, $end = 16, $unit = 'px', $default = NULL) {
  $range = '';
  if (is_numeric($start) && is_numeric($end)) {
    $range = array();
    $size = $start;
    while ($size >= $start && $size <= $end) {
      if ($size == $default) {
        $range[$size . $unit] = $size . $unit .' (default)';
      }
      else {
        $range[$size . $unit] = $size . $unit;
      }
      $size++;
    }
  }
  return $range;
}
